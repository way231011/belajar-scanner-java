import java.util.Scanner;

public class ScannerMethod {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukan angka = ");
        int a = sc.nextInt();
        System.out.println("Belanja " + a + hasil(a));
    }

    static String hasil(int a) {
        if (a >= 1000000) {
            return " Selamat Anda Mendapakan Hadiah Motor";
        } else if (a >= 500000) {
            return " Selamat Anda Mendapatkan Hadiah Belender";
        } else if (a >= 250000) {
            return " Selamat Anda Mendapakan Voucher";
        }else {
            return " Maaf Anda Belum Mendapatkan Hadiah";
        }

    }
}
